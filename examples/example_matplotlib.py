from matplotlib import pyplot as plt
import numpy as np

# monkeypatch figure
plt.oldfigure = plt.figure


def newfigure(*args, **kwargs):
    ret = plt.oldfigure(*args, **kwargs)
    plt.figtext(0.01, 0.01, "reproduce: {{ hash }}", size=10)
    return ret


plt.figure = newfigure
# end monkeypatch

X = np.linspace(0, 10, 100)
Y = np.sin(X)

fig, ax = plt.subplots(1, 1)
ax.plot(X, Y)
plt.savefig('test_matplotlib.png')
plt.show()
plt.close(fig)

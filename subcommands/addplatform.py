"""usage: reproduce addplatform [--force] [--help] <regex> <type>

  --force   overwrite an existing platform
  --help    print this information

For common options for 'reproduce' see 'reproduce --help'.

"""


def run(commands, config, inifile):
    regex = commands['<regex>']
    platformtype = commands['<type>']
    force = commands['--force']
    if 'platforms' in config and regex in config['platforms'] and not force:
        print('Warning: Regex already exist, use --force to overwrite it')
    else:
        if 'platforms' not in config:
            config['platforms'] = {}
        config['platforms'][regex] = platformtype
        with open(inifile, 'w+') as configfile:
            config.write(configfile)

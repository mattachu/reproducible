from docopt import docopt
import sys
import pytest

from importlib.machinery import SourceFileLoader
reproduce = SourceFileLoader('reproduce', 'reproduce').load_module()


def test_hello_world(capfd):
    commands = docopt(reproduce.__doc__,
                      argv=['run', '{} examples/example_hello_world.py'.format(sys.executable)],
                      options_first=True)
    reproduce.main(commands)
    captured = capfd.readouterr()
    assert captured.out == 'hello world\n'


def test_hello_world2(capfd):
    commands = docopt(reproduce.__doc__,
                      argv=['run', '-p', 'who:Me',
                            '--template', 'examples/example_hello_world_2.py',
                            '{} examples/example_hello_world_2.py'.format(sys.executable)],
                      options_first=True)
    reproduce.main(commands)
    captured = capfd.readouterr()
    assert captured.out == 'hello Me\n'


def test_hello_world2_no_template(capfd):
    commands = docopt(reproduce.__doc__,
                      argv=['run', '-p', 'who:Me',
                            '{} examples/example_hello_world_2.py'.format(sys.executable)],
                      options_first=True)
    reproduce.main(commands)
    captured = capfd.readouterr()
    assert captured.out == 'hello Me\n'


def test_hello_world2_missing_parameter(caplog):
    commands = docopt(reproduce.__doc__,
                      argv=['run',
                            '--template', 'examples/example_hello_world_2.py',
                            '{} examples/example_hello_world_2.py'.format(sys.executable)],
                      options_first=True)
    with pytest.raises(SystemExit):
        reproduce.main(commands)
    assert 'Parameter "who" did not get a value' in caplog.text
